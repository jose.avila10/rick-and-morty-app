import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/src/app.dart';

void main(){
  runApp(
    MaterialApp(
      home: App()
    )
  );
}