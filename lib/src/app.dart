// ignore_for_file: prefer_const_constructors
// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';

import 'package:rick_and_morty_app/src/screens/characters.dart' as characters;
import 'package:rick_and_morty_app/src/screens/locations.dart' as locations;
import 'package:rick_and_morty_app/src/screens/episodes.dart' as episodes;

class App extends StatelessWidget {
  var WidgetSpaceCol = 25.0;
  var textSizeOnButton = 20.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Rick & Morty App"),
      ),
      body: 
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/rick_and_morty_wallpaper_1.jpeg"),
              fit: BoxFit.cover,
            )
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Center(
                child: Column(
                  children: [
                    RaisedButton(
                      color: Colors.transparent,
                      textColor: Colors.white,
                      child: 
                        Text(
                          "Characters",
                          style: TextStyle(fontSize: textSizeOnButton),
                        ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => characters.Characters()
                          )
                        );
                      },
                    ),
                    SizedBox(height: WidgetSpaceCol),
                    RaisedButton(
                      color: Colors.transparent,
                      textColor: Colors.white,
                      child: 
                      Text(
                          "Locations",
                          style: TextStyle(fontSize: textSizeOnButton),
                        ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => locations.Locations()
                          )
                        );
                      },
                    ),
                    SizedBox(height: WidgetSpaceCol),
                    RaisedButton(
                      color: Colors.transparent,
                      textColor: Colors.white,
                      child: 
                        Text(
                          "Episodes",
                          style: TextStyle(fontSize: textSizeOnButton),
                        ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => episodes.Episodes()
                          )
                        );
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
    );
  }
}