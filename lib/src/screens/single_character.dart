// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'loading.dart';
import 'package:rick_and_morty_app/src/screens/single_location.dart' as singleLocation;
import 'package:rick_and_morty_app/src/screens/single_episode.dart' as singleEpisode;

class SingleCharacter extends StatefulWidget {
  final String url;
  SingleCharacter({required this.url});

  @override
  _SingleCharacterState createState() => new _SingleCharacterState();
}

class _SingleCharacterState extends State<SingleCharacter> {
  Map? data;
  var character_info;

  var data_episodes;
  var episodes = [];
  var totalEpisodes;
  var loadedEpisodes;

  void getCharacterInfo() async {
    try{
      http.Response response = await http.get(Uri.parse(widget.url));
      data = json.decode(response.body);
      setState(() {
        character_info = data;
      });
    }
    catch(e){
      Navigator.pop(context);
    }

    for(int i = 0; i < character_info['episode'].length; i++){
      //character_info['episode'][i]
      http.Response response_episode = await http.get(Uri.parse(character_info['episode'][i]));
      data_episodes = json.decode(response_episode.body);
      setState(() {
         episodes.add(data_episodes);
      });

      setState(() {
        totalEpisodes = character_info['episode'].length;
        loadedEpisodes = episodes.length;
      });
    }

  }

  @override
  void initState() {
    super.initState();
    getCharacterInfo();
  }

  @override
  Widget build(BuildContext context) {
    final children = <Widget>[];
    if(character_info != null){
      // var characterEpisodesData = character_info['episode'];
      // List<String> characterEpisodes = List<String>.from(characterEpisodesData);
      //print(character_info['episode']);
    }

    return character_info == null
      ? Loading()
      : Scaffold(
        appBar: AppBar(
          flexibleSpace: Container(
          decoration: 
            BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/rick_and_morty_banner.jpg"),
                fit: BoxFit.cover,
              ),
            ),
        ),
          title: Text(character_info['name']),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 10,
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Center(
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Image(
                          image: NetworkImage(character_info['image']),
                          width: 200.0,
                          ),
                        )
                      ),
                    ),
                    Text(character_info['name'],
                      style: const TextStyle(
                        fontSize: 30.0, fontWeight: FontWeight.bold)
                    ),
                    Text(
                      "Status: " + character_info['status'],
                      style: TextStyle(fontSize: 30.0),
                    ),
                    Text(
                      "Species: " + character_info['species'],
                      style: TextStyle(fontSize: 30.0),
                    ),
                    Text(
                      "Gender: " + character_info['gender'],
                      style: TextStyle(fontSize: 30.0),
                    ),
                    GestureDetector(
                      child: Text(
                        "Location: " + character_info['location']['name'],
                        style: TextStyle(fontSize: 25.0),
                      ),
                      onTap: () {
                        if(character_info['location']['name'] != "unknown"){
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => singleLocation.SingleLocation(url: character_info['location']['url'])
                            )
                          );
                        }
                      },
                    ),
                  ],
                ),
              )
            ),
            
            Visibility(
              visible: totalEpisodes != null
                          && loadedEpisodes != null
                            && totalEpisodes != loadedEpisodes
                        ? true
                        : false,
              child: Expanded(
                flex: 5,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Image(
                          width: 100.0,
                          image: AssetImage("assets/images/loading_circle_2.gif"),
                        )
                      ),
                      Container(
                        child: Text( totalEpisodes == null || loadedEpisodes == null
                          ? "Loading episodes list"
                          : loadedEpisodes.toString() 
                              + " episodes of " + totalEpisodes.toString()
                                + " loaded.",
                          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)
                        )
                      )
                    ]
                  ),
                ),
              ),
            ),
            
            Expanded(
              flex: totalEpisodes != null
                          && loadedEpisodes != null
                            && totalEpisodes != loadedEpisodes
                        ? 5
                        : 10,
              child: ListView.builder(
                itemCount: episodes == null ? 0 : episodes.length,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index){
                  return Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        children: <Widget>[
                          GestureDetector(
                            child: SizedBox(
                              width: double.infinity,
                              child: Text(
                                "Season: " + episodes[index]['episode'][1]
                                  + episodes[index]['episode'][2]
                                  + ", Episode: " + episodes[index]['episode'][4]
                                    + episodes[index]['episode'][5]
                                    + ": " + episodes[index]['name'],
                                textAlign: TextAlign.left,
                                ),
                            ),
                            onTap: (){
                              Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => singleEpisode.SingleEpisode(url: character_info['episode'][index])
                            )
                          );
                            },
                          ),
                        ],
                      ),
                    )
                  );
                }
              ),
            ),
          ],
        )
      );
  }
}
