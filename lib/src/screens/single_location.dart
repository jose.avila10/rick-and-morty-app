// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'loading.dart';
import 'package:rick_and_morty_app/src/screens/single_character.dart' as singleCharacter;

class SingleLocation extends StatefulWidget {
  final String url;
  SingleLocation({required this.url});

  @override
  _SingleLocationState createState() => new _SingleLocationState();
 }

class _SingleLocationState extends State<SingleLocation> {
    Map? data;
    var location_info;
    
    var data_residents;
    var residents = [];
    var totalResidents;
    var loadedResidents;


  void getLocationInfo() async {
    try{
      http.Response response = await http.get(Uri.parse(widget.url));
      data = json.decode(response.body);
      setState(() {
        location_info = data;
      });
    }
    catch(e){
      Navigator.pop(context);
    }

    for(int i = 0; i < location_info['residents'].length; i++){
      http.Response response_residents = await http.get(Uri.parse(location_info['residents'][i]));
      data_residents = json.decode(response_residents.body);
      setState(() {
         residents.add(data_residents);

         totalResidents = location_info['residents'].length;
         loadedResidents = residents.length;
      });
    }
    
  }

  @override
  void initState() {
    super.initState();
    getLocationInfo();
  }

  @override
  Widget build(BuildContext context) {
    return location_info == null
    ? Loading()
    : Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: 
            BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/rick_and_morty_banner.jpg"),
                fit: BoxFit.cover,
              ),
            ),
        ),
        title: Text(location_info['name']),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 5,
            child: SingleChildScrollView(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        location_info['name'],
                        style: const TextStyle(
                        fontSize: 30.0, fontWeight: FontWeight.bold)
                      ),
                      Text(
                        "Type: ",
                        style: const TextStyle(
                        fontSize: 30.0, fontWeight: FontWeight.bold)
                      ),
                      Text(location_info['type'],
                        style: const TextStyle(fontSize: 30.0)
                      ),
                      Text(
                        "Dimension: ",
                        style: const TextStyle(
                        fontSize: 30.0, fontWeight: FontWeight.bold)
                      ),
                      Text(
                        location_info['dimension'],
                        style: const TextStyle(fontSize: 30.0)
                      ),
                      Text("Residents:",
                        style: const TextStyle(
                        fontSize: 30.0, fontWeight: FontWeight.bold)
                      ),
                    ],
                  ),
                ),
              )
            ),
          ),

          Visibility(
              visible: totalResidents != null
                          && loadedResidents != null
                            && totalResidents != loadedResidents
                        ? true
                        : false,
              child: Expanded(
                flex: 5,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Image(
                          width: 100.0,
                          image: AssetImage("assets/images/loading_circle_2.gif"),
                        )
                      ),
                      Container(
                        child: Text( totalResidents == null || loadedResidents == null
                          ? "Loading residents list"
                          : loadedResidents.toString() 
                              + " residents of " + totalResidents.toString()
                                + " loaded.",
                          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)
                        )
                      )
                    ]
                  ),
                ),
              ),
            ),

          Expanded(
            flex: 10,//30 original
            child: ListView.builder(
              //itemCount: location_info['residents'] == null ? 0 : location_info['residents']?.length,
              itemCount: residents == null ? 0 : residents.length,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index){
                return Card(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      children: <Widget>[
                        GestureDetector(
                          child: Row(
                            children: <Widget>[
                              CircleAvatar(
                                backgroundImage:
                                  NetworkImage(residents[index]['image']),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 20.0),
                                child: Text(
                                  residents[index]['name'],
                                  style: residents[index]['name'].length <= 25 ? const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold) : const TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),
                                ),
                              )
                            ],
                          ),
                          onTap: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => singleCharacter.SingleCharacter(url: location_info['residents'][index])
                              )
                            );
                          },
                        ),

                      ],
                    ),
                  )
                );
              }
            ),
          ),
        ],
      )
    );
  }
}