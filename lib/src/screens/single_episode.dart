// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'loading.dart';
import 'package:rick_and_morty_app/src/screens/single_character.dart' as singleCharacter;


class SingleEpisode extends StatefulWidget {
  @override
  final String url;
  SingleEpisode({required this.url});

  _SingleEpisodeState createState() => new _SingleEpisodeState();
 }

class _SingleEpisodeState extends State<SingleEpisode> {
  Map? data;
  var episode_info;

  var data_characterOnEpisode;
  var charcatersOnEpisodes = [];
  var totalCharacterOnEpisode;
  var loadedCharacterOnEpisode;

  void getEpisodeInfo() async {
    try{
      http.Response response = await http.get(Uri.parse(widget.url));
      data = json.decode(response.body);
      setState(() {
        episode_info = data;
      });
    }
    catch(e){
      Navigator.pop(context);
    }

    for(int i = 0; i < episode_info['characters'].length; i++){
      //character_info['episode'][i]
      http.Response response_episode = await http.get(Uri.parse(episode_info['characters'][i]));
      data_characterOnEpisode = json.decode(response_episode.body);
      setState(() {
         charcatersOnEpisodes.add(data_characterOnEpisode);

        totalCharacterOnEpisode = episode_info['characters'].length;
        loadedCharacterOnEpisode = charcatersOnEpisodes.length;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getEpisodeInfo();
  }

  @override
  Widget build(BuildContext context) {
    return episode_info == null
      ? Loading()
      : Scaffold(
        appBar: AppBar(
          flexibleSpace: Container(
          decoration: 
            BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/rick_and_morty_banner.jpg"),
                fit: BoxFit.cover,
              ),
            ),
        ),
          title: Text(episode_info['name']),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 10,
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Text(episode_info['name'],
                      style: const TextStyle(
                        fontSize: 30.0, fontWeight: FontWeight.bold)
                    ),
                    Text(
                      "Original air date:",
                      style: TextStyle(fontSize: 30.0),
                    ),
                    Text(
                      "Status: " + episode_info['air_date'],
                      style: TextStyle(fontSize: 30.0),
                    ),
                    Text(
                      "Episode: " + episode_info['episode'].substring(4, 6),
                      style: TextStyle(fontSize: 30.0),
                    ),
                    Text(
                      "Season: " + episode_info['episode'].substring(1, 3),
                      style: TextStyle(fontSize: 30.0),
                    ),
                  ],
                ),
              )
            ),

            Visibility(
              visible: totalCharacterOnEpisode != null
                          && loadedCharacterOnEpisode != null
                            && totalCharacterOnEpisode != loadedCharacterOnEpisode
                        ? true
                        : false,
              child: Expanded(
                flex: 10,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Image(
                          width: 100.0,
                          image: AssetImage("assets/images/loading_circle_2.gif"),
                        )
                      ),
                      Container(
                        child: Text( totalCharacterOnEpisode == null || loadedCharacterOnEpisode == null
                          ? "Loading characters list"
                          : loadedCharacterOnEpisode.toString() 
                              + " characters of " + totalCharacterOnEpisode.toString()
                                + " loaded.",
                          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)
                        )
                      )
                    ]
                  ),
                ),
              ),
            ),

            Expanded(
              flex: 30,//30 original
              child: ListView.builder(
                //itemCount: location_info['residents'] == null ? 0 : location_info['residents']?.length,
                itemCount: charcatersOnEpisodes == null ? 0 : charcatersOnEpisodes.length,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index){
                  return Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        children: <Widget>[
                          GestureDetector(
                            child: Row(
                              children: <Widget>[
                                CircleAvatar(
                                  backgroundImage:
                                    NetworkImage(charcatersOnEpisodes[index]['image']),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 20.0),
                                  child: Text(
                                    charcatersOnEpisodes[index]['name'],
                                    style: charcatersOnEpisodes[index]['name'].length <= 25 ? const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold) : const TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),
                                  ),
                                )
                              ],
                            ),
                            onTap: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => singleCharacter.SingleCharacter(url: charcatersOnEpisodes[index]['url'])
                                )
                              );
                            },
                          ),

                        ],
                      ),
                    )
                  );
                }
              ),
            ),

          ],
        )
      );
  }
}