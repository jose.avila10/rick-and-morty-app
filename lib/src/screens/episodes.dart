// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'loading.dart';
import 'package:rick_and_morty_app/src/screens/single_episode.dart' as singleEpisode;

class Episodes extends StatefulWidget {
  @override
  _EpisodesState createState() => new _EpisodesState();
}

class _EpisodesState extends State<Episodes> {
  Map? data;
  List? episodes;

  var totalEpisodes;
  var loadedEpisodes;

  getCharacters() async {
    http.Response firstResponse =
        await http.get(Uri.parse('https://rickandmortyapi.com/api/episode'));
    var firstdata = json.decode(firstResponse.body);
    int totalPages = firstdata['info']['pages'];

    for (int i = 1; i <= totalPages; i++) {
      http.Response response = await http.get(Uri.parse(
          'https://rickandmortyapi.com/api/episode?page=' + i.toString()));
      data = json.decode(response.body);
      setState(() {
        episodes = [...?episodes, ...data?['results']];

        totalEpisodes = data?['info']['count'];
        loadedEpisodes = episodes?.length;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getCharacters();
  }

  @override
  Widget build(BuildContext context) {
    return episodes == null 
      ? Loading()
      : Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: 
            BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/rick_and_morty_banner.jpg"),
                fit: BoxFit.cover,
              ),
            ),
        ),
        title: Text("Episodes"),
      ),
      body: Column(
        children: [


          Visibility(
            visible: totalEpisodes != null
                        && loadedEpisodes != null
                          && totalEpisodes != loadedEpisodes
                      ? true
                      : false,
            child: Expanded(
              flex: 10,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Image(
                        width: 100.0,
                        image: AssetImage("assets/images/loading_circle_2.gif"),
                      )
                    ),
                    Container(
                      child: Text( totalEpisodes == null || loadedEpisodes == null
                        ? "Loading Episodes"
                        : loadedEpisodes.toString() 
                            + " episodes of " + totalEpisodes.toString()
                              + " loaded.",
                        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)
                      )
                    )
                  ]
                ),
              ),
            ),
          ),


          Expanded(
            flex: 30,
            child: ListView.builder(
              itemCount: episodes == null ? 0 : episodes?.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  child: GestureDetector(
                    onTap: () {
                      //print(episodes![index]['url']);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => singleEpisode.SingleEpisode(url: episodes![index]['url'])
                        )
                      );
                    },
                    child: Column(
                      children: [
                        Row(
                          children: <Widget>[
                            Container(
                              constraints: const BoxConstraints(minWidth: 40.0),
                              child: Text(
                                episodes![index]['id'].toString(),
                                style: const TextStyle(fontSize: 15.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                episodes![index]['name'],
                                style: episodes![index]['name'].length <= 25 ? const TextStyle(fontSize: 20.0,) : const TextStyle(fontSize: 15.0,),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              constraints: const BoxConstraints(minWidth: 40.0),
                              child: const Text(
                                "",
                              ),
                            ),
                            const Text(
                              "Air Date: ",
                              style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              episodes![index]['air_date'],
                              //style: episodes![index]['name'].length <= 25 ? const TextStyle(fontSize: 20.0,) : const TextStyle(fontSize: 15.0,),
                              style: const TextStyle(fontSize: 15.0),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              constraints: const BoxConstraints(minWidth: 40.0),
                              child: const Text(
                                "",
                              ),
                            ),
                            const Text(
                              "Episode: ",
                              //style: episodes![index]['name'].length <= 25 ? const TextStyle(fontSize: 20.0,) : const TextStyle(fontSize: 15.0,),
                              style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              episodes![index]['episode'][4] + episodes![index]['episode'][5] + ", ",
                              style: const TextStyle(fontSize: 15.0),
                            ),
                            const Text(
                              "Season: ",
                              //style: episodes![index]['name'].length <= 25 ? const TextStyle(fontSize: 20.0,) : const TextStyle(fontSize: 15.0,),
                              style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              episodes![index]['episode'][1] + episodes![index]['episode'][2],
                              //style: episodes![index]['name'].length <= 25 ? const TextStyle(fontSize: 20.0,) : const TextStyle(fontSize: 15.0,),
                              style: const TextStyle(fontSize: 15.0),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      )
    );
  }
}
