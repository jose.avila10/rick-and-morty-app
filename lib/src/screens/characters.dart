// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:rick_and_morty_app/src/screens/loading.dart';
import 'dart:async';
import 'dart:convert';

import 'loading.dart';
import 'package:rick_and_morty_app/src/screens/single_character.dart' as singleCharacter;

class Characters extends StatefulWidget {
  @override
  _CharactersState createState() => new _CharactersState();
}

class _CharactersState extends State<Characters> {
  Map? data;
  List? characters;

  var totalCharacters;
  var loadedCharacters;

  getCharacters() async {
    try{
      http.Response firstResponse =
          await http.get(Uri.parse('https://rickandmortyapi.com/api/character'));
      var firstdata = json.decode(firstResponse.body);
      int totalPages = firstdata['info']['pages'];

      for (int i = 1; i <= totalPages; i++) {
        http.Response response = await http.get(Uri.parse(
            'https://rickandmortyapi.com/api/character?page=' + i.toString()));
        data = json.decode(response.body);
        setState(() {
          characters = [...?characters, ...data?['results']];

          totalCharacters = data?['info']['count'];
          loadedCharacters = characters?.length;
        });
      }
    }
    catch(e){
      Navigator.pop(context);
    }
  }

  @override
  void initState() {
    super.initState();
    getCharacters();
  }

  @override
  Widget build(BuildContext context) {
    return characters == null
      ? Loading()
      : Scaffold(
          appBar: AppBar(
            flexibleSpace: Container(
              decoration: 
                BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/rick_and_morty_banner.jpg"),
                    fit: BoxFit.cover,
                  ),
                ),
            ),
            title: Text("Characters"),
          ),
          body: Column(
            children: [
              
              Visibility(
              visible: totalCharacters != null
                          && loadedCharacters != null
                            && totalCharacters != loadedCharacters
                        ? true
                        : false,
              child: Expanded(
                flex: 10,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Image(
                          width: 100.0,
                          image: AssetImage("assets/images/loading_circle_2.gif"),
                        )
                      ),
                      Container(
                        child: Text( totalCharacters == null || loadedCharacters == null
                          ? "Loading Characters"
                          : loadedCharacters.toString() 
                              + " characters of " + totalCharacters.toString()
                                + " loaded.",
                          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)
                        )
                      )
                    ]
                  ),
                ),
              ),
            ),

              Expanded(
                flex: 30,
                child: ListView.builder(
                  itemCount: characters == null ? 0 : characters?.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => singleCharacter.SingleCharacter(url: characters![index]['url'])
                            )
                          );
                        },
                        child: Column(
                          children: [
                            Row(
                              children: <Widget>[
                                Container(
                                  constraints: const BoxConstraints(minWidth: 40.0),
                                  child: Text(
                                    characters![index]['id'].toString(),
                                    style: const TextStyle(fontSize: 15.0),
                                  ),
                                ),
                                CircleAvatar(
                                  backgroundImage:
                                      NetworkImage(characters![index]['image']),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text(
                                    characters![index]['name'],
                                    style: characters![index]['name'].length <= 25 ? const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold) : const TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  constraints: const BoxConstraints(minWidth: 40.0),
                                  child: const Text(""),
                                ),
                                const Text(
                                  "Status: ",
                                  style: TextStyle(fontSize: 15.0),
                                ),
                                Icon(
                                  characters![index]['status'] == "Alive" ? Icons.favorite : 
                                    characters![index]['status'] == "Dead" ? Icons.sentiment_very_dissatisfied_sharp  : 
                                      characters![index]['status'] == "unknown" ? Icons.question_mark_rounded : 
                                        Icons.alarm,
              
                                    color: characters![index]['status'] == "Alive" ? Colors.redAccent : 
                                    characters![index]['status'] == "Dead" ? Colors.black  : 
                                      characters![index]['status'] == "unknown" ? Colors.black : 
                                        Colors.black,
              
                                    size: 20.0
                                  ),
                                Text(
                                  " Species: ",
                                  style: const TextStyle(fontSize: 15.0),
                                ),
                                Text(
                                  characters![index]['species'],
                                  style: const TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                                  )
              
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          )
        );
  }
}
