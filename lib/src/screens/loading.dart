// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return(
      Scaffold(
        appBar: AppBar(
          flexibleSpace: Container(
            decoration: 
              BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/rick_and_morty_banner.jpg"),
                  fit: BoxFit.cover,
                ),
              ),
          ),
          title: Text("Loading"),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/black_circle_loading.webp"),
              //fit: BoxFit.cover,
            ),
            color: Colors.black,
          ),
        )
      )
    );
  }
}