import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'loading.dart';
import 'package:rick_and_morty_app/src/screens/single_location.dart' as singleLocation;

class Locations extends StatefulWidget {
  @override
  _LocationsState createState() => new _LocationsState();
}

class _LocationsState extends State<Locations> {
  Map? data;
  List? locations;

  var totalLocations;
  var loadedLocations;

  getCharacters() async {
    http.Response firstResponse =
        await http.get(Uri.parse('https://rickandmortyapi.com/api/location'));
    var firstdata = json.decode(firstResponse.body);
    int totalPages = firstdata['info']['pages'];

    for (int i = 1; i <= totalPages; i++) {
      http.Response response = await http.get(Uri.parse(
          'https://rickandmortyapi.com/api/location?page=' + i.toString()));
      data = json.decode(response.body);
      setState(() {
        locations = [...?locations, ...data?['results']];

        totalLocations = data?['info']['count'];
        loadedLocations = locations?.length;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getCharacters();
  }

  @override
  Widget build(BuildContext context) {
    return locations == null 
      ? Loading()
      : Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: 
            BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/rick_and_morty_banner.jpg"),
                fit: BoxFit.cover,
              ),
            ),
        ),
        title: Text("Locations"),
      ),
      body: Column(
        children: [

          Visibility(
            visible: totalLocations != null
                        && loadedLocations != null
                          && totalLocations != loadedLocations
                      ? true
                      : false,
            child: Expanded(
              flex: 10,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Image(
                        width: 100.0,
                        image: AssetImage("assets/images/loading_circle_2.gif"),
                      )
                    ),
                    Container(
                      child: Text( totalLocations == null || loadedLocations == null
                        ? "Loading Locations"
                        : loadedLocations.toString() 
                            + " locations of " + totalLocations.toString()
                              + " loaded.",
                        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)
                      )
                    )
                  ]
                ),
              ),
            ),
          ),

          Expanded(
            flex: 30,
            child: ListView.builder(
              itemCount: locations == null ? 0 : locations?.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => singleLocation.SingleLocation(url: locations![index]['url'])
                        )
                      );
                    },
                    child: Column(
                      children: [
                        Row(
                          children: <Widget>[
                            Container(
                              constraints: const BoxConstraints(minWidth: 40.0),
                              child: Text(
                                locations![index]['id'].toString(),
                                style: const TextStyle(fontSize: 15.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                locations![index]['name'],
                                style: locations![index]['name'].length <= 25 ? const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold) : const TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              constraints: const BoxConstraints(minWidth: 40.0),
                              child: const Text(""),
                            ),
                            const Text(
                              "Type: ",
                              style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              locations![index]['type'],
                              style: const TextStyle(fontSize: 15.0),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              constraints: const BoxConstraints(minWidth: 40.0),
                              child: const Text(""),
                            ),
                            const Text(
                              "Dimension: ",
                              style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              locations![index]['dimension'],
                              style: const TextStyle(fontSize: 15.0),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      )
    );
  }
}
